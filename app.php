<?php
//var_dump('app.php');
$app=['link'=>null,'items'=>null, 'err'=>null];
require_once ('errors.php');
switch($_SERVER['REQUEST_METHOD']){
    case 'POST':
        //var_dump('post');
        require_once('controller.php');
        if(empty($app['err'])) require ('parser.php');
        require_once (__DIR__.'/views/header.templ.php');
        require_once (__DIR__.'/views/form.templ.php');
        if(empty($app['err']))require_once (__DIR__.'/views/table.templ.php');
        require_once (__DIR__.'/views/footer.templ.php');
        break;
    case 'GET':
        //var_dump('get');
        require_once (__DIR__.'/views/header.templ.php');
        require_once(__DIR__.'/views/form.templ.php');
        require_once (__DIR__.'/views/footer.templ.php');
        break;
}
//var_dump($app);