<?php
require_once 'phpQuery-onefile.php';
try{
//var массив текущего селектора класса для списка товаров
$items_class=['type'=>'','value'=>''];

//var массив товаров
/*[
 * ['title'=>'','price'=>''],...
 * ]
 */
$items=[];

//var селекторы классов товара YandexMarket
$items_short_classes = [
    ['catalog','.n-snippet-','cell2'],
    ['catalog','.n-snippet-','card2'],
    ['menu','._3Sl8p9JEFz','._2n8iG_3PT6','.name._3rufvyYddw._2oOs9ACcmk._3qQ4L4GpT7','._2zV4WptnUC ._1d0RZz_YOo']
];

//begin получение ссылки
$link = $app['link'];
//end получение ссылки

//begin получение html страницы (объект phpQuery)
if(!UR_exists($link)) throw new Exception(EHTML);
$html = file_get_contents($link);
$pqhtml = phpQuery::newDocument($html);
//end получение html страницы (объект phpQuery)

//begin определение текущего класса товара на странице YandexMarket
foreach($items_short_classes as $items_short_class){
    if (empty(pq($items_short_class[1].$items_short_class[2])->html()))
        continue;
    else{
        $items_class=['type'=>$items_short_class[0],'value'=>$items_short_class[1].$items_short_class[2]];
        break;
    }
}
//end определение текущего класса товара на странице YandexMarket

//begin формирование списка товаров
$pqitems = pq($items_class['value']);
//end формирование списка товаров

//begin выборка информации от товаре из списка товаров
foreach($pqitems as $item){
    switch ($items_class['type']){
        case 'catalog' :
            $title=pq($item)->find($items_class['value'].'__header '.$items_class['value'].'__title a')->attr('title');
            $price=pq($item)->find($items_class['value'].'__main-price-wrapper .price')->text();
            break;
        case 'menu' :
            $title=pq($item)->find($items_short_classes[2][3])->text();
            $price=pq($item)->find($items_short_classes[2][4])->text();
            break;
    }
    if(!(empty($title) or empty($price))){
        $items[]=['title'=>$title,'price'=>$price];
        /*echo '<pre>';
        var_dump($title);
        var_dump($price);
        echo '</pre>';*/
    }
}
if(empty($items))throw new Exception(EITEMS);
    $app['items'] = $items;
//end выборка информации от товаре из списка товаров
}catch(Exception $e){
    $app['err']=$e->getMessage();
};

function UR_exists($url){
   $headers=get_headers($url);
   return stripos($headers[0],"200 OK")?true:false;
}