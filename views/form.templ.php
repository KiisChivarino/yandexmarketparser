<div class="container">
    <form method="post" action="">
        <div class="form-group">
            <label for="exampleInputEmail1">Ссылка на каталог ЯндексМаркет</label>
            <input type="url" class="form-control" id="inputURL"  name="link" placeholder="Ссылка на каталог ЯндексМаркет">
        </div>
        <button type="submit" class="btn btn-primary">Получить список товаров</button>
    </form>
</div>
<?php if(!empty($app['err'])):?>
<div class="container">
    <div class="alert alert-danger" role="alert">
        <?=$app['err']?>
    </div>
</div>
<? endif;?>