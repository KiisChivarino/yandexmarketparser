<div class="container">
    <table class="table">
        <caption>
            Товары по ссылке
        </caption>
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Название</th>
            <th scope="col">Цена</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($app['items'] as $key=>$item):?>
        <tr>
            <th scope="row"><?=$key+1?></th>
            <td><?=$item['title']?></td>
            <td><?=$item['price']?></td>
        </tr>
        <? endforeach; ?>
        </tbody>
    </table>
</div>